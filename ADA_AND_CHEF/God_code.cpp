#include <iostream>

using namespace std;

int main()
{
    int T, N;
    
    //cout<<"Enter number of Test Cases: ";
    cin>>T;
    
    for(int j=0; j<T; ++j)
    {
        //cout<<"Enter number of Dishes: ";
        cin>>N;
        
        int C[N], i, k, a=0, b=0, temp;
        //cout<<"Enter the time for each dish: ";
        for(i=0; i<N; ++i)
        {
            cin>>temp;
            for(k=i-1; k>=0; --k)
                if(C[k]<temp)
                    C[k+1]=C[k];
                else
                    break;
            C[k+1]=temp;
        }
        
        for(i=0; i<N; ++i)
        {
            //cout<<"C["<<i<<"]="<<C[i]<<endl;
            if(i==0 || a<=b)
                a+=C[i];
            else if(i==1 || a>b)
                b+=C[i];
                
            //cout<<"a = "<<a<<endl<<"b = "<<b<<endl;
        }
        
        if(a>=b)
            cout<<a<<endl;
        else
            cout<<b<<endl;
    }

    return 0;
}