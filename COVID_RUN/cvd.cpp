#include <iostream>
using namespace std;

#define SIKUN 498347589347598

#define debug 1

int main() {
	int T,N,K,X,Y;
#ifdef SOMU
	int flag;
#endif
	cin>>T;
	
    for(int i=1;i<=T;i++)
    {
	    cin>>N;
	    cin>>K;
	    cin>>X;
	    cin>>Y;
	   
#ifdef SIKUN
	    
	    #if debug
	        cout<<"Executing Sikun's Code.\n";
	    #endif
	    
	    int c[N];
	    for(int j=0;j<N;++j)
	    {
	        c[j]=0;
	    }
        while(c[X]!=1)
        {
            c[X]=1;
            X=(X+K)%N;
        }
        if(c[Y]==1)
#endif
        
#ifdef SOMU
        
        #if debug
            cout<<"Executing Somu's Code.\n";
        #endif
        
	    flag = 0;
        for(int j=(X+K)%N; j!=X; j=(j+K)%N)
            if(Y == j)
            {
                flag = 1;
                break;
            }
        if(flag == 1)
#endif
        
            cout<<"YES\n";
        else
            cout<<"NO\n";
    }
	
	return 0;
}
